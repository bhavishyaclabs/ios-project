//
//  SecondViewController.swift
//  To Do List
//
//  Created by apple on 20/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {   

    @IBOutlet weak var toDoItem: UITextField!
    @IBAction func addItem(sender: AnyObject) {
   
        //adds new item at the end...
        if toDoItem.text != "" {
        toDoItems.append(toDoItem.text)
    }
        
        // let pi = 3.14 // immutable - > which never changes...
        let fixedToDoItems = toDoItems //immutable
        println(fixedToDoItems)
        
        //for permanent storage...
        NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey:"toDoItems")
        NSUserDefaults.standardUserDefaults().synchronize()
        println (NSUserDefaults.standardUserDefaults().objectForKey("toDoItems"))
        
        self.view.endEditing(true)
        
        if toDoItem.text != "" {
        
        //gives an alert message...
        var alert = UIAlertController(title: "Alert", message: "Items Added", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
        
        toDoItem.text = ""     //empties the text field when item added...
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool { //keyboard managing function...
        
        toDoItem.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { //hides keyboard if clicked on empty space.
        
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

