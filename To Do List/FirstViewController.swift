//
//  FirstViewController.swift
//  To Do List
//
//  Created by apple on 20/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

var toDoItems:[String] = []

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tasksTable: UITableView! //table view

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
    
        return toDoItems.count             //returns number of rows or cells...
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        //it takes cell values in table...
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell.textLabel?.text = toDoItems[indexPath.row]
        return cell
    }                                  //cell displaying function...
    
    override func viewWillAppear(animated: Bool) {
            
            if var storedtoDoItems: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems") {
            toDoItems = []
            for var i = 0; i < storedtoDoItems.count; ++i {
                toDoItems.append(storedtoDoItems[i] as NSString)
            }
        }
        
        //reloads data in table
        tasksTable.reloadData()
}                                     //cell adding function...

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.Delete) {
            toDoItems.removeAtIndex(indexPath.row)
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }                                 //cell removing function... //slide on data and pess delete
}