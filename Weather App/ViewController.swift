//
//  ViewController.swift
//  Weather App
//
//  Created by apple on 28/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBOutlet weak var cityLabel: UITextField!
    @IBOutlet weak var messageDisplayed: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var overView: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var imageSlider: UIImageView!
    
    @IBAction func buttonPressed(sender: AnyObject) {
        self.view.endEditing(true)
        self.messageDisplayed.hidden = false
        //URL from where data is extracted...
        var urlString = "http://www.weather-forecast.com/locations/" + cityLabel.text.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
        var url = NSURL(string: urlString)
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
        var urlContent = NSString(data: data, encoding: NSUTF8StringEncoding)
        println(urlContent)
            
            if NSString(string: urlContent!).containsString("<span class=\"phrase\">") {
                //For cropping of the sentences according to need...
                var contentArray = urlContent!.componentsSeparatedByString("<span class=\"phrase\">")
                var newContentArray = contentArray[1].componentsSeparatedByString("</span")
                var weatherForcast = newContentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "º")
                var max = weatherForcast.componentsSeparatedByString("max")
                var maximum = max[1].componentsSeparatedByString("on")
                var min = weatherForcast.componentsSeparatedByString("min")
                var minimum = min[1].componentsSeparatedByString("on")
                var over = weatherForcast.componentsSeparatedByString("")
                var overview = weatherForcast.componentsSeparatedByString(".")
                var change = weatherForcast as NSString
                
                dispatch_async(dispatch_get_main_queue()) {  // for stopping other processes and resuming this...
                    //For displaying in labels...
                    self.messageDisplayed.text = weatherForcast
                    self.messageDisplayed.hidden = false   //for visibility of hidden background...
                    self.maxTemp.text = "\(maximum[0])"
                    self.minTemp.text = "\(minimum[0])"
                    self.overView.text = "\(overview[0])"
                    self.overView.hidden = false
                    
                    //For changing background image according to temprature...
                    if change.containsString("dry") == true {
                        var image = UIImage(named:"dry.jpg")
                        self.imageSlider.image = image
                        
                    } else if change.containsString("rain") == true {
                        var image = UIImage(named:"rainy.png")
                        self.imageSlider.image = image
                        
                    } else if change.containsString("clear") == true {
                        var image = UIImage(named:"clear.png")
                        self.imageSlider.image = image
                        
                    } else if change.containsString("cloudy") == true {
                        var image = UIImage(named:"cloudy.jpg")
                        self.imageSlider.image = image
                        
                    } else if change.containsString("snow") {
                        var image = UIImage(named:"snow.jpg")
                        self.imageSlider.image = image
                        
                    } else {
                        var image = UIImage(named:"horses.png")
                        self.imageSlider.image = image
                    }
                }
            } else {
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.messageDisplayed.text = " couldn't find that city"  //if wrong or no city is entered...
                    self.messageDisplayed.hidden = false
                }
            }
        }
        task.resume()  //for continuing the task...
        cityLabel.text = ""
    }
      func textFieldShouldReturn(cityLabel: UITextField!) -> Bool {
        
        cityLabel.resignFirstResponder()
        return true
    }
      override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        self.view.endEditing(true)
    }
    
}




