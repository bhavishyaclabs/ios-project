//
//  ViewController.swift
//  Stopwatch
//
//  Created by Rob Percival on 01/07/2014.
//  Copyright (c) 2014 Appfish. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lapOne: UILabel!
    
    @IBOutlet weak var labelOneHidden: UILabel!
    
        var timer = NSTimer()
    var checkPlay = false
    
    @IBOutlet var time : UILabel!
    var mSecCount = 0
    var secCount = 0
    var minCount = 0
    
    @IBAction func play(sender : AnyObject) {
        
        if checkPlay == false{
        
        
          timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("result"), userInfo: nil, repeats: true)
            
            checkPlay = true
        }
    }
    
    @IBAction func pause(sender : AnyObject) {
        timer.invalidate()
       checkPlay = false
    }
    
    @IBOutlet weak var reloadOutlet: UIButton!
    
    @IBAction func reloadButton(sender: AnyObject) {
        

        var mCount = 0
        var sCount = 0
        var mSCount = 0
        labelOneHidden.text = String(mCount) + ":" + String(sCount) + ":" + String(mSCount)
        
    }
    @IBAction func reset(sender : AnyObject) {
        timer.invalidate()
        mSecCount = 0
        secCount = 0
        minCount = 0
        time.text="0:0:0"
        checkPlay =  false
    }
    
    @IBAction func saveButton(sender: AnyObject) {
        
        labelOneHidden.text = String(minCount) + ":" + String(secCount) + ":" + String(mSecCount)
        
        labelOneHidden.hidden = false
        lapOne.hidden = false
        reloadOutlet.hidden = false
        
        
           }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
    }
    
    func result() {
        
        mSecCount++
        
        if mSecCount > 9 {    //checks mili seconds...
            
            secCount++
            mSecCount = 0
        }
        
        
        
        
    if secCount  > 59 {       //checks seconds...
    
       minCount++
        secCount = 0// increases minute counter...
    }
    
        time.text = String(minCount) + ":" + String(secCount) + ":" + String(mSecCount)
    
    
    
    if minCount == 59 {
    
    timer.invalidate()
     checkPlay =  false
    }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

