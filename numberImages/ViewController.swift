//
//  ViewController.swift
//  numberImages
//
//  Created by apple on 14/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    
    @IBOutlet weak var imageSlider: UIImageView!
    
    @IBAction func goButton(sender: AnyObject) { //SUBMITTING THE RESULTS..
        
        self.view.endEditing(true)
        
        var number = textField.text
        
        switch number { //FOR VIEWING DIFFRENT IMAGES...
            
        case "1":

            var image = UIImage(named:"image1.jpg")
            imageSlider.image = image
            
        case "2":
            
            var image = UIImage(named:"image2.jpg")
            imageSlider.image = image
    
        case "3":
            
            var image = UIImage(named:"image3.jpg")
            imageSlider.image = image
        case "4":
            
            var image = UIImage(named:"image4.jpg")
            imageSlider.image = image
        case "5":
            
            var image = UIImage(named:"image5.jpg")
            imageSlider.image = image
            
        case "6":
            
            var image = UIImage(named:"image6.jpg")
            imageSlider.image = image
            
        case "7":
            
            var image = UIImage(named:"image7.jpg")
            imageSlider.image = image
            
        case "8":
            
            var image = UIImage(named:"image8.jpeg")
            imageSlider.image = image
            
        case "9":
            
            var image = UIImage(named:"image9.JPG")
            imageSlider.image = image
            
        case "10":
            
            var image = UIImage(named:"image10.gif")
            imageSlider.image = image
            
        default:     //IF 0 OR EMPTY VIEW THIS IMAGE
            
            var image = UIImage(named:"image11.gif")
            imageSlider.image = image
        }
    
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

