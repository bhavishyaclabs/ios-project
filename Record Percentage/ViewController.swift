//
//  ViewController.swift
//  Record Percentage
//
//  Created by apple on 14/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

var englishMarks = 0
var hindiMarks = 0
var mathsMarks = 0
var scienceMarks = 0
var sstMarks = 0

class ViewController: UIViewController {

    @IBOutlet weak var enterName: UITextField!
    
    @IBOutlet weak var enterAge: UITextField!
    
    @IBOutlet weak var enterStandard: UITextField!
    
    @IBOutlet weak var englishMarks: UITextField!
    
    @IBOutlet weak var hindiMarks: UITextField!
    
    @IBOutlet weak var mathsMarks: UITextField!
    
    @IBOutlet weak var scienceMarks: UITextField!
    
    @IBOutlet weak var totalMarks: UILabel!
    
    @IBOutlet weak var percentageMarks: UILabel!
    
    @IBOutlet weak var sstMarks: UITextField!
    
    @IBOutlet weak var imageViewer: UIImageView!
    
    
    //to submit the information...
        @IBAction func submitButon(sender: AnyObject) {
        
            if enterName.text.isEmpty {
                
                enterName.text = "enter name"
                
                }
            if(enterStandard.text.isEmpty) {
                
                enterStandard.text = "enter standard"
                    
            }
           if (enterAge.text.isEmpty) {
                
                enterAge.text = "enter age"
            }
            
            // Declarations...
        var english = englishMarks.text.toInt()
        var hindi = hindiMarks.text.toInt()
        var maths = mathsMarks.text.toInt()
        var science = scienceMarks.text.toInt()
        var sst = sstMarks.text.toInt()
         
            
            //entering the marks validation...
            if (englishMarks.text.isEmpty) {
                
                englishMarks.text = " enter marks "
                
            }
            if hindiMarks.text.isEmpty {
                
                hindiMarks.text = " enter marks "
                
            }
            if mathsMarks.text.isEmpty {
                
                mathsMarks.text = " enter marks "
                
            }
            if  scienceMarks.text.isEmpty {
                
                scienceMarks.text = " enter marks "
                
            }
            if sstMarks.text.isEmpty {
                
                sstMarks.text = "enter marks"
                
            } else { //calculation of marks...
        var total = english! + hindi! + maths! + science! + sst!
        totalMarks.text = "\(total)"
        
                // calculation of percentage...
        var percentage = (total / 5)
        percentageMarks.text = "\(percentage)%"
        
            if percentage <= 50 {
                var image = UIImage(named:"image9.jpg")
               imageViewer.image = image
                
            } else if percentage > 50 && percentage <= 75 {
                
                var image = UIImage(named:"image2.jpg")
                imageViewer.image = image
            } else if percentage > 75 && percentage <= 100 {
                
                var image = UIImage(named:"image1.jpg")
                imageViewer.image = image
            }
        
        }
    
    
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


