//
//  ViewController.swift
//  calculator again
//
//  Created by apple on 15/01/15.
//  Copyright (c) 2015 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var calculatorDisplay: UILabel!
   
    var TypingNumber = false
    var firstNumber = 0.0
    var secondNumber = 0.0
    var operation = ""
    var dotPressed = false
    var operationClicked = false
    var operationContinued = false // to handle continuous operations
    var result = 0.0
    
    @IBAction func point(sender: AnyObject) { //point values...
        
            if dotPressed == false {
                var num = "."
                
                if TypingNumber {
                    calculatorDisplay.text = calculatorDisplay.text! + num
                } else {
                    calculatorDisplay.text = num
                    TypingNumber = true}
            }
        
         dotPressed = true
            
        }
    

    @IBAction func clearButoon(sender: AnyObject)  { //FOR CLEARING
       
        calculatorDisplay.text = ""
        operation = ""
        TypingNumber = false
        firstNumber = 0.0
        secondNumber = 0.0
        dotPressed = false
        operationContinued = false
        result = 0.0
        operationClicked = false
        
    }
    
    @IBAction func calculationTapped(sender: AnyObject) { //CALCULATIONS...
        
        dotPressed = false
        TypingNumber = false
        firstNumber = (calculatorDisplay.text! as NSString).doubleValue
        
       operation = sender.currentTitle!!
        
        
    }
    
    @IBAction func numberTapped(sender: AnyObject) { //ENTERING NUMBERS...
        
      
        
        var number = sender.currentTitle
        
        if TypingNumber {
            calculatorDisplay.text = calculatorDisplay.text! + number!!
        } else {
            calculatorDisplay.text! = number!!
            TypingNumber = true
        }
    }
    
    @IBAction func equals(sender: AnyObject) {  // RESULT CALCULATED
        TypingNumber = false
        //dotPressed = false
        var result = 0.0
        secondNumber = (calculatorDisplay.text! as NSString).doubleValue
        
        if operation == "+" {
            result = firstNumber + secondNumber
        } else if operation == "-" {
            result = self.firstNumber - self.secondNumber
            
        } else if operation == "*" {
            
            result = self.firstNumber * self.secondNumber
            
        } else if operation == "/" {
            
            result = self.firstNumber / self.secondNumber
                
            }
    

        calculatorDisplay.text = "\(result)"
        operationClicked = false
        operationContinued = false
        dotPressed = false
    }
    



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

